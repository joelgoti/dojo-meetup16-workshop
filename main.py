# Libraries used for the web application
from flask import Flask, request, jsonify


# Parameters to config the web application
app = Flask(__name__)
app.config['DEBUG'] = True


# Test data to use
movies = [
    {'id': 0,
     'title': 'The Lord of the Rings: The Fellowship of the Ring',
     'year_published': '2001'},
    {'id': 1,
     'title': 'The Lord of the Rings: The Two Towers',
     'year_published': '2002'},
    {'id': 2,
     'title': 'The Lord of the Rings: The Return of the King',
     'year_published': '2003'}
]


# Initial placeholder
@app.route('/', methods=['GET'])
def index():
    return "<h1>Comunidad Dojo</h1><p>Meetup #16</p>"


# API endpoint to get all movies from our test data
@app.route('/api/movies', methods=['GET'])
def get_all_movies():
    return jsonify(movies)


if __name__ == "__main__":
    app.run()
